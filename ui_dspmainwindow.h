/********************************************************************************
** Form generated from reading UI file 'dspmainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DSPMAINWINDOW_H
#define UI_DSPMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_DSPMainWindow
{
public:
    QWidget *centralWidget;
    QCustomPlot *plot;
    QLabel *Sinewave1;
    QLabel *Sinewave2;
    QLabel *Sinewave3;
    QLabel *Sinewave5;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    QLineEdit *lineEdit_4;
    QLineEdit *lineEdit_5;
    QLabel *Sinewave4;
    QPushButton *submitButton;
    QCustomPlot *plot_2;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *DSPMainWindow)
    {
        if (DSPMainWindow->objectName().isEmpty())
            DSPMainWindow->setObjectName(QStringLiteral("DSPMainWindow"));
        DSPMainWindow->resize(1057, 546);
        centralWidget = new QWidget(DSPMainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        plot = new QCustomPlot(centralWidget);
        plot->setObjectName(QStringLiteral("plot"));
        plot->setGeometry(QRect(20, 160, 361, 261));
        Sinewave1 = new QLabel(centralWidget);
        Sinewave1->setObjectName(QStringLiteral("Sinewave1"));
        Sinewave1->setGeometry(QRect(30, 10, 61, 16));
        Sinewave2 = new QLabel(centralWidget);
        Sinewave2->setObjectName(QStringLiteral("Sinewave2"));
        Sinewave2->setGeometry(QRect(30, 30, 51, 16));
        Sinewave3 = new QLabel(centralWidget);
        Sinewave3->setObjectName(QStringLiteral("Sinewave3"));
        Sinewave3->setGeometry(QRect(30, 50, 51, 16));
        Sinewave5 = new QLabel(centralWidget);
        Sinewave5->setObjectName(QStringLiteral("Sinewave5"));
        Sinewave5->setGeometry(QRect(30, 90, 51, 16));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(112, 10, 121, 20));
        lineEdit_2 = new QLineEdit(centralWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(112, 30, 121, 20));
        lineEdit_3 = new QLineEdit(centralWidget);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(112, 50, 121, 20));
        lineEdit_4 = new QLineEdit(centralWidget);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(112, 70, 121, 20));
        lineEdit_5 = new QLineEdit(centralWidget);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));
        lineEdit_5->setGeometry(QRect(112, 90, 121, 20));
        Sinewave4 = new QLabel(centralWidget);
        Sinewave4->setObjectName(QStringLiteral("Sinewave4"));
        Sinewave4->setGeometry(QRect(30, 70, 51, 16));
        submitButton = new QPushButton(centralWidget);
        submitButton->setObjectName(QStringLiteral("submitButton"));
        submitButton->setGeometry(QRect(394, 40, 101, 23));
        plot_2 = new QCustomPlot(centralWidget);
        plot_2->setObjectName(QStringLiteral("plot_2"));
        plot_2->setGeometry(QRect(400, 160, 411, 261));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(400, 80, 91, 23));
        DSPMainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(DSPMainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1057, 21));
        DSPMainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(DSPMainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        DSPMainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(DSPMainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        DSPMainWindow->setStatusBar(statusBar);

        retranslateUi(DSPMainWindow);

        QMetaObject::connectSlotsByName(DSPMainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *DSPMainWindow)
    {
        DSPMainWindow->setWindowTitle(QApplication::translate("DSPMainWindow", "DSPMainWindow", Q_NULLPTR));
        Sinewave1->setText(QApplication::translate("DSPMainWindow", "Sinewave1", Q_NULLPTR));
        Sinewave2->setText(QApplication::translate("DSPMainWindow", "Sinewave2", Q_NULLPTR));
        Sinewave3->setText(QApplication::translate("DSPMainWindow", "Sinewave3", Q_NULLPTR));
        Sinewave5->setText(QApplication::translate("DSPMainWindow", "Sinewave5", Q_NULLPTR));
        lineEdit->setText(QApplication::translate("DSPMainWindow", "100", Q_NULLPTR));
        lineEdit_2->setText(QApplication::translate("DSPMainWindow", "200", Q_NULLPTR));
        lineEdit_3->setText(QApplication::translate("DSPMainWindow", "300", Q_NULLPTR));
        lineEdit_4->setText(QApplication::translate("DSPMainWindow", "400", Q_NULLPTR));
        lineEdit_5->setText(QApplication::translate("DSPMainWindow", "500", Q_NULLPTR));
        Sinewave4->setText(QApplication::translate("DSPMainWindow", "Sinewave4", Q_NULLPTR));
        submitButton->setText(QApplication::translate("DSPMainWindow", "Submit", Q_NULLPTR));
        pushButton->setText(QApplication::translate("DSPMainWindow", "SumbitPeGraf2", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DSPMainWindow: public Ui_DSPMainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DSPMAINWINDOW_H
