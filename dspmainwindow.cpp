#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DSPMainWindow)
{	


	
    ui->setupUi(this);
	QObject::connect(ui->submitButton, SIGNAL(clicked()), this, SLOT(Submit_Click()));
	QObject::connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(Submit_Click2()));

}

void DSPMainWindow:: Submit_Click()
{
	ui->plot->clearGraphs();
	QVector<double>xAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}
	QVector<double> yAxis(44100);

	int s1, s2, s3, s4, s5;

	s1 = ui->lineEdit->text().toInt();
	s2 = ui->lineEdit_2->text().toInt();
	s3 = ui->lineEdit_3->text().toInt();
	s4 = ui->lineEdit_4->text().toInt();
	s5 = ui->lineEdit_5->text().toInt();
	double a, b, c, d, e;
	double f_lp = 500;
	for (int i = 0; i < 44100; i++)
	{
		a = 1 * sin(2 * M_PI * s1 * xAxis[i]);
		b = 1 * sin(2 * M_PI * s2 * xAxis[i]);
		c = 1 * sin(2 * M_PI * s3 * xAxis[i]);
		d = 1 * sin(2 * M_PI * s4* xAxis[i]);
		e = 1 * sin(2 * M_PI * s5 * xAxis[i]);
		yAxis[i] = a + b + c + d + e;
	}
	Aquila::SpectrumType filterSpectrum(64);
	for (std::size_t i = 0; i < 64; ++i)
	{
		if (i < (64 * f_lp / 44100))
		{
			// passband
			filterSpectrum[i] = 1.0;
		}
		else
		{
			// stopband
			filterSpectrum[i] = 0.0;
		}
	}

	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(xAxis, yAxis);
	ui->plot->xAxis->setRange(0, 0.1);
	ui->plot->yAxis->setRange(-10, 10);

	ui->plot->replot();
}


void DSPMainWindow::Submit_Click2()
{
	ui->plot_2->clearGraphs();
	QVector<double>xAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}
	QVector<double> yAxis(44100);

	int s1, s2, s3, s4, s5;

	s1 = ui->lineEdit->text().toInt();
	s2 = ui->lineEdit_2->text().toInt();
	s3 = ui->lineEdit_3->text().toInt();
	s4 = ui->lineEdit_4->text().toInt();
	s5 = ui->lineEdit_5->text().toInt();
	double a, b, c, d, e;
	double f_lp = 500;
	for (int i = 0; i < 44100; i++)
	{
		a = 1 * sin(2 * M_PI * s1 * xAxis[i]);
		b = 1 * sin(2 * M_PI * s2 * xAxis[i]);
		c = 1 * sin(2 * M_PI * s3 * xAxis[i]);
		d = 1 * sin(2 * M_PI * s4* xAxis[i]);
		e = 1 * sin(2 * M_PI * s5 * xAxis[i]);
		yAxis[i] = a + b + c + d + e;
	}
	Aquila::SpectrumType filterSpectrum(64);
	for (std::size_t i = 0; i < 64; ++i)
	{
		if (i < (64 * f_lp / 44100))
		{
			// passband
			filterSpectrum[i] = 1.0;
		}
		else
		{
			// stopband
			filterSpectrum[i] = 0.0;
		}
	}

	ui->plot_2->addGraph();
	ui->plot_2->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot_2->graph(0)->setData(xAxis, yAxis);
	ui->plot_2->xAxis->setRange(0, 0.1);
	ui->plot_2->yAxis->setRange(-10, 10);

	ui->plot_2->replot();
}


DSPMainWindow::~DSPMainWindow()
{
    delete ui;
}
